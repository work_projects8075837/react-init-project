import react from '@vitejs/plugin-react-swc';
import path from 'path';
import { defineConfig } from 'vite';
import { VitePWA } from 'vite-plugin-pwa';
import tsconfigPaths from 'vite-tsconfig-paths';
import { manifestForPlugin } from './manifest.pwa';

export default defineConfig({
    build: {
        chunkSizeWarningLimit: 1600,
    },
    resolve: {
        alias: { '@': path.resolve(__dirname, './src') },
    },
    plugins: [react(), tsconfigPaths(), VitePWA(manifestForPlugin)],
    define: {
        VITE_BASE_DOMAIN: process.env.VITE_BASE_DOMAIN,
    },
});
