import { VitePWAOptions } from 'vite-plugin-pwa';

export const manifestForPlugin: Partial<VitePWAOptions> = {
    registerType: 'autoUpdate',
    includeAssets: [
        '/pwa/favicon.ico',
        '/pwa/apple-touch-icon.png',
        '/pwa/maskable_icon.png',
    ],
    manifest: {
        name: 'УРВ',
        short_name: 'УРВ',
        description: 'Флагман техподдержка',
        icons: [
            {
                src: '/pwa/android-chrome-192x192.png',
                sizes: '192x192',
                type: 'image/png',
            },
            {
                src: '/pwa/android-chrome-512x512.png',
                sizes: '512x512',
                type: 'image/png',
            },
            {
                src: '/pwa/apple-touch-icon.png',
                sizes: '180x180',
                type: 'image/png',
            },
            {
                src: '/pwa/maskable_icon.png',
                sizes: '225x225',
                type: 'image/png',
                purpose: 'maskable',
            },
        ],
        screenshots: [
            {
                src: '/pwa/screen1.png',
                sizes: '1920x1080',
                type: 'image/png',
                form_factor: 'wide',
                label: 'Welcome page',
            },
            {
                src: '/pwa/screen2.png',
                sizes: '1920x1080',
                type: 'image/png',
                form_factor: 'wide',
                label: 'Main page',
            },
            {
                src: '/pwa/screen3.png',
                sizes: '1920x1080',
                type: 'image/png',
                form_factor: 'wide',
                label: 'Ticket view page',
            },
            {
                src: '/pwa/narrow_screen1.png',
                sizes: '540x674',
                type: 'image/png',
                form_factor: 'narrow',
                label: 'Contacts view page',
            },
        ],
        theme_color: '#1E293B',
        background_color: '#FAFAFA',
        display: 'standalone',
        scope: '/',
        id: '/',
        start_url: '/',
        orientation: 'portrait',
    },
};
