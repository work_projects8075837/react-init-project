import { queryClient } from '@/shared/lib';
import { ToasterWithOptions } from '@/shared/ui';
import { QueryClientProvider } from '@tanstack/react-query';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';

interface ProvidersProps {
    children: React.ReactNode;
}

export const Providers: React.FC<ProvidersProps> = ({ children }) => {
    return (
        <>
            <BrowserRouter>
                <ToasterWithOptions />
                <QueryClientProvider client={queryClient}>
                    {children}
                </QueryClientProvider>
            </BrowserRouter>
        </>
    );
};
