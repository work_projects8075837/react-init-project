import '@/shared/style/base.css';
import '@/shared/style/base.scss';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Router } from './Router.tsx';
import { Providers } from './providers/index.ts';

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <Providers>
            <Router />
        </Providers>
    </React.StrictMode>,
);
