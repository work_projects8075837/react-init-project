import { QueryClient } from '@tanstack/react-query';

export const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnMount: false,
        },
    },
});

export const invalidateQuery = (key: string | string[]) => {
    queryClient.invalidateQueries({
        queryKey: typeof key === 'string' ? [key] : key,
        exact: false,
    });
};

export const clearKey = (key: string | string[]) => {
    queryClient.removeQueries({
        queryKey: typeof key === 'string' ? [key] : key,
        exact: false,
    });
    invalidateQuery(key);
};
