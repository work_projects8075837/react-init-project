const isQueryExist = <T>(value: T) => {
    return value || value === false || value === null;
};

const isWorkKey = (key: string) => {
    return !['STORAGE_PREFIX'].includes(key);
};

export const generateQuery = <T>(queryObject?: T) => {
    let queryString = '?';

    for (const key in queryObject) {
        if (isQueryExist(queryObject[key]) && isWorkKey(key)) {
            const value = queryObject[key];

            if (value instanceof Array) {
                if (value.length > 0) {
                    const existValues = value.filter((v) => isQueryExist(v));
                    queryString += `${key}=${existValues.join(';')}&`;
                }
            } else {
                queryString += `${key}=${value}&`;
            }
        }
    }

    queryString = queryString.slice(0, -1);

    return queryString;
};
