export interface ITokenData {
    sub: string;
    exp: number;
    roles?: string[];
}
