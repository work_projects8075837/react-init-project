import { jwtDecode } from 'jwt-decode';
import { ITokenData } from './types';

export const parseJwt = (token: string): ITokenData => {
    return jwtDecode<ITokenData>(token);
};
