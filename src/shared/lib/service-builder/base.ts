import { PaginatedData, getUserId } from '@/shared/model';
import { ApiManager } from '../api-manager';
import { ServiceHookOptions, useAuthQuery, useDebounce } from '../hooks';
import { generateQuery } from '../http/generate-query';
import { queryClient } from '../query-client';
import { ActionsFromBuilder } from './types';

export class BaseServiceBuilder<TDefaultFilter> {
    constructor(protected serviceManager: ApiManager) {}

    private createKeys(route: string) {
        const upperRoute = route.toUpperCase();

        const keys = {
            GET_ALL_KEY: `GET_ALL_${upperRoute}_KEY`,
            GET_ONE_KEY: `GET_ONE_${upperRoute}_KEY`,
        };

        return keys;
    }

    protected createService<
        TEntity,
        TCreateEntity,
        TPag = unknown,
        TFilter = unknown,
        TDetailEntity = TEntity,
        TUpdateEntity = Partial<TCreateEntity>,
    >({
        route,
        searchFields,
    }: {
        readonly route: string;
        searchFields: Array<keyof TFilter>;
    }) {
        const serviceManager = this.serviceManager;

        const keys = this.createKeys(route);

        const api = {
            async getAll<T = Partial<TPag & TFilter & TDefaultFilter>>(
                query?: T,
            ) {
                return serviceManager.get<PaginatedData<TEntity[]>>(
                    `/${route}/${generateQuery(query)}`,
                    { isAuth: true },
                );
            },

            async create(data: TCreateEntity) {
                return serviceManager.post<TDetailEntity>(`/${route}/`, data, {
                    isAuth: true,
                });
            },

            async getOne(id: string) {
                return serviceManager.get<TDetailEntity>(`/${route}/${id}/`, {
                    isAuth: true,
                });
            },

            async update(id: string, data: TUpdateEntity) {
                return serviceManager.patch<TDetailEntity>(
                    `/${route}/${id}/`,
                    data,
                    {
                        isAuth: true,
                    },
                );
            },

            async delete(id: string) {
                return serviceManager.delete(`/${route}/${id}/`, {
                    isAuth: true,
                });
            },
        };

        const useListQuery = <R>(
            queryFn: () => Promise<R>,
            query?: Partial<TPag & TFilter & TDefaultFilter>,
            options?: ServiceHookOptions,
        ) => {
            return useAuthQuery({
                mainKey: keys.GET_ALL_KEY,
                variablesKeys: { ...query },
                ...options,
                queryFn,
            });
        };

        const useOneQuery = <R>(
            queryFn: () => Promise<R>,
            id?: string,
            options?: ServiceHookOptions,
        ) => {
            return useAuthQuery({
                mainKey: keys.GET_ONE_KEY,
                variablesKeys: { id },
                enabled: !!id,
                ...options,
                queryFn,
            });
        };

        const hooks = {
            useList: (
                query?: Partial<TPag & TFilter & TDefaultFilter>,
                options?: ServiceHookOptions,
            ) => {
                return useListQuery(
                    async () => {
                        return await api.getAll(query).then((res) => res.data);
                    },
                    query,
                    options,
                );
            },

            useOne: (id?: string, options?: ServiceHookOptions) => {
                return useOneQuery(
                    async () => {
                        if (!id) {
                            throw Error(
                                `${route.toUpperCase()} - id is undefined. enabled was not provided`,
                            );
                        }
                        return await api.getOne(id).then((res) => res.data);
                    },
                    id,
                    options,
                );
            },

            useSearch: (
                search?: string | null,
                filter?: Partial<TPag & TFilter & TDefaultFilter>,
                options?: ServiceHookOptions,
            ) => {
                const debouncedSearch = useDebounce(search, 500);

                const searchFilter = searchFields.reduce((filter, field) => {
                    return { ...filter, [field]: debouncedSearch };
                }, {});

                const allFilter = { ...filter, ...searchFilter };

                return useAuthQuery({
                    mainKey: keys.GET_ALL_KEY,
                    variablesKeys: { ...allFilter },
                    ...options,
                    queryFn: async () => {
                        return await api
                            .getAll({
                                is_hard:
                                    Object.keys(allFilter).length > 1
                                        ? false
                                        : true,
                                ...allFilter,
                            })
                            .then((res) => res.data);
                    },
                });
            },
        };

        const actions: ActionsFromBuilder<TEntity, TDetailEntity> = {
            updateOne: (updater, id) => {
                queryClient.setQueryData<TDetailEntity>(
                    [keys.GET_ONE_KEY, { userId: getUserId(), id }],
                    (data) => {
                        if (!data) return data;
                        updater(data);
                    },
                );
            },
            updateList: (updater) => {
                queryClient.setQueryData<PaginatedData<TEntity[]>>(
                    [keys.GET_ALL_KEY, { userId: getUserId() }],
                    (paginatedData) => {
                        if (!paginatedData) return paginatedData;

                        return {
                            ...paginatedData,
                            data: updater(paginatedData.data),
                        };
                    },
                );
            },
        };
        return {
            api,
            hooks,
            actions,
            keys,
            lib: { useListQuery, useOneQuery },
        };
    }
}
