export interface DefaultFilter {
    is_hide: boolean;
}

export type UpdaterCallback<T> = (data: T) => T;

export type OneEntityAction<TDetailEntity> = (
    updater: UpdaterCallback<TDetailEntity>,
    id?: string,
) => void;

export type ListEntityAction<TEntity> = (
    updater: UpdaterCallback<TEntity[]>,
) => void;

export interface ActionsFromBuilder<TEntity, TDetailEntity = TEntity> {
    updateOne: OneEntityAction<TDetailEntity>;
    updateList: ListEntityAction<TEntity>;
}
