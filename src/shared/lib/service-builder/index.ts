import { apiManager } from '..';
import { ServiceBuilder } from './service-builder';
import { DefaultFilter } from './types';

export const serviceBuilder = new ServiceBuilder<DefaultFilter>(apiManager);
