import { useEffect, useState } from 'react';

export function useDebounce<T>(value: T, delay = 500, isDisable = false): T {
    const [debouncedValue, setDebouncedValue] = useState<T>(value);

    useEffect(() => {
        const timer = setTimeout(() => {
            if (debouncedValue !== value && !isDisable) {
                setDebouncedValue(value);
            }
        }, delay);

        return () => {
            clearTimeout(timer);
        };
    }, [value, delay, isDisable]);

    return debouncedValue;
}
