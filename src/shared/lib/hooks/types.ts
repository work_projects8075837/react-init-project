export type ServiceHookVariables = {
    [key: string]: unknown | undefined;
};

export interface ServiceHookOptions {
    refetchOnMount?: boolean;
}
