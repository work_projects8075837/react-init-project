import { ServiceHookVariables, useAuthQuery } from '..';
import { useDebounce } from '../use-debounce/useDebounce';

interface UseSearchQueryProps<R> {
    mainKey: string;
    search?: string;
    searchFn: (search?: string) => Promise<R>;
    variablesKeys?: ServiceHookVariables;
}

export const useSearchQuery = <R>({
    mainKey,
    search,
    searchFn,
    variablesKeys,
}: UseSearchQueryProps<R>) => {
    const debouncedSearch = useDebounce(search, 500);

    const query = useAuthQuery({
        mainKey,
        variablesKeys: { search: debouncedSearch, ...variablesKeys },
        queryFn: async () => {
            return await searchFn(debouncedSearch);
        },
    });

    return query;
};
