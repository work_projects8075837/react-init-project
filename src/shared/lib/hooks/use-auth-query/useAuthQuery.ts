import { useAuthorizationQuery } from '@/shared/model';
import { useQuery } from '@tanstack/react-query';
import { UseAuthorizedQueryProps } from './types';

export const useAuthQuery = <R>({
    refetchOnMount = false,
    mainKey,
    variablesKeys,
    queryFn,
    enabled,
}: UseAuthorizedQueryProps<R>) => {
    const { data } = useAuthorizationQuery();

    const query = useQuery({
        queryKey: [
            mainKey,
            {
                userId: data?.userId,
                ...variablesKeys,
            },
        ],
        enabled: !!data?.userId && enabled,
        queryFn,
        refetchOnMount,
    });

    return query;
};
