import { ServiceHookOptions, ServiceHookVariables } from '..'

export interface UseAuthorizedQueryProps<R> extends ServiceHookOptions {
    mainKey: string | symbol;
    variablesKeys?: ServiceHookVariables;
    queryFn: () => Promise<R>;
    enabled?: boolean;
}