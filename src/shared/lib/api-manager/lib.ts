type IData = {
    [key: string]: unknown | undefined | null;
};

export const validateExistValues = <D>(data?: D) => {
    if (!data) {
        return data;
    }
    const newData: IData = { ...data };
    for (const key in data) {
        if (
            !data[key] &&
            data[key] !== false &&
            data[key] !== null &&
            data[key] !== 0
        ) {
            newData[key] = undefined;
        } else if (data[key] === null) {
            newData[key] = null;
        }
    }

    return newData;
};
