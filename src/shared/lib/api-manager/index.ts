import { BASE_DOMAIN, GET_AUTHORIZATION_KEY } from '@/shared/config/constants';
import axios from 'axios';
import { TCookiesStorage, cookieStorage } from '../cookie-storage/index';
import { isDevDomain } from '../http/index';
import { invalidateQuery } from '../query-client';
import { ApiManager } from './api-manger';

export const apiInstance = axios.create({
    baseURL: `http${isDevDomain(BASE_DOMAIN) ? '' : 's'}://${BASE_DOMAIN}/api`,
});

const apiManager = new ApiManager<TCookiesStorage>(
    apiInstance,
    cookieStorage,
    '/auth/refresh/',
    () => {
        invalidateQuery(GET_AUTHORIZATION_KEY);
    },
);

export { ApiManager, apiManager };
