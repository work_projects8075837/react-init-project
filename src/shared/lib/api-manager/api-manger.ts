import { AxiosInstance, AxiosResponse } from 'axios';
import { validateExistValues } from './lib';
import {
    ApiManagerRequestConfig,
    AuthorizeResult,
    CredentialsStorage,
} from './types';

export class ApiManager<
    TCredentialsStorage extends CredentialsStorage = CredentialsStorage,
> {
    constructor(
        private _api: AxiosInstance,
        private _credentialsStorage: TCredentialsStorage,
        private _authUrl: string,
        private onUnauthorize: () => void,
    ) {}

    public async authorize() {
        const refresh = this._credentialsStorage.getItem('refresh');
        if (!refresh) {
            this.unauthorize();
            throw Error();
        }

        const access = await this._api
            .post<AuthorizeResult>(
                this._authUrl,
                {},
                { headers: { Authorization: `Bearer ${refresh}` } },
            )
            .then((response) => response.data.access)
            .catch(async () => {
                this.unauthorize();

                throw Error();
            });

        return access;
    }

    public unauthorize() {
        this._credentialsStorage.removeItem('refresh');
        this._credentialsStorage.removeItem('access');
        this.onUnauthorize();
    }

    public getCredentialsStorage() {
        return this._credentialsStorage;
    }

    private async setAuthHeaders<D>(config?: ApiManagerRequestConfig<D>) {
        if (!config?.isAuth) {
            return;
        }

        if (!this._credentialsStorage.getItem('access')) {
            const access = await this.authorize();
            this._credentialsStorage.setItem('access', access);
        }
        const access = this._credentialsStorage.getItem('access');

        if (config.headers) {
            config.headers.Authorization = `Bearer ${access}`;
        } else {
            config.headers = { Authorization: `Bearer ${access}` };
        }
    }

    private async createAuthMiddlewareEndpoint<R, D>(
        axiosPromise: (
            url: string,
            config?: ApiManagerRequestConfig<D>,
            data?: D,
        ) => Promise<R>,
        url: string,
        config?: ApiManagerRequestConfig<D>,
        data?: D,
    ): Promise<R> {
        this.setAuthHeaders(config);
        const query = async (
            url: string,
            config?: ApiManagerRequestConfig<D>,
            data?: D,
        ): Promise<R> => {
            return axiosPromise(url, config, data).catch(async (error) => {
                if (error?.response?.status === 401 && config?.isAuth) {
                    const access = await this.authorize();
                    this._credentialsStorage.setItem('access', access);
                    const newConfig = {
                        ...config,
                        headers: {
                            ...config?.headers,
                            Authorization: `Bearer ${access}`,
                        },
                    };
                    return query(url, newConfig, data);
                }
                throw error;
            });
        };

        return query(url, config, data);
    }

    public async post<T = unknown, R = AxiosResponse<T>, D = unknown>(
        url: string,
        data?: D,
        config?: ApiManagerRequestConfig<D>,
    ): Promise<R> {
        const customPost = (
            url: string,
            config?: ApiManagerRequestConfig<D>,
            data?: D,
        ): Promise<R> => {
            return this._api.post(url, validateExistValues(data), config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customPost,
            url,
            config,
            data,
        );
    }

    public async patch<T = unknown, R = AxiosResponse<T>, D = unknown>(
        url: string,
        data?: D,
        config?: ApiManagerRequestConfig<D>,
    ): Promise<R> {
        const customPost = (
            url: string,
            config?: ApiManagerRequestConfig<D>,
            data?: D,
        ): Promise<R> => {
            return this._api.patch(url, validateExistValues(data), config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customPost,
            url,
            config,
            data,
        );
    }

    public async delete<T = unknown, R = AxiosResponse<T>, D = unknown>(
        url: string,
        config?: ApiManagerRequestConfig<D>,
    ): Promise<R> {
        const customDelete = (
            url: string,
            config?: ApiManagerRequestConfig<D>,
        ): Promise<R> => {
            return this._api.delete(url, config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customDelete,
            url,
            config,
        );
    }

    public async get<T = unknown, R = AxiosResponse<T>, D = unknown>(
        url: string,
        config?: ApiManagerRequestConfig<D>,
    ): Promise<R> {
        const customGet = (
            url: string,
            config?: ApiManagerRequestConfig<D>,
        ): Promise<R> => {
            return this._api.get(url, config);
        };

        return await this.createAuthMiddlewareEndpoint<R, D>(
            customGet,
            url,
            config,
        );
    }
}
