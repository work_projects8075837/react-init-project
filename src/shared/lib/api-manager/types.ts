import {
    Axios,
    AxiosDefaults,
    AxiosHeaderValue,
    AxiosRequestConfig,
    AxiosResponse,
    HeadersDefaults,
} from 'axios';

export interface AuthorizeResult {
    access: string;
}

export interface ApiManagerRequestConfig<D> extends AxiosRequestConfig<D> {
    isAuth?: boolean;
}

export interface ApiManagerAxiosInstance extends Axios {
    <T = unknown, R = AxiosResponse<T>, D = unknown>(
        config: ApiManagerRequestConfig<D>,
    ): Promise<R>;
    <T = unknown, R = AxiosResponse<T>, D = unknown>(
        url: string,
        config?: ApiManagerRequestConfig<D>,
    ): Promise<R>;

    defaults: Omit<AxiosDefaults, 'headers'> & {
        headers: HeadersDefaults & {
            [key: string]: AxiosHeaderValue;
        };
    };
}

export interface CredentialsStorage {
    setItem: (key: string, value: string) => void;
    getItem: (key: string) => string | undefined;
    removeItem: (key: string) => void;
}
