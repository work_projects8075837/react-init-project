import { websocketConsumerBuilder } from './base';

type TMessageKey = 'user';

type TMessage = {
    type: 'user';
    data: {
        user_id: string;
    };
};

export const wsEventsConsumer =
    websocketConsumerBuilder.createWebsocketConsumer<TMessageKey, TMessage>({
        handlers: {
            user: () => {},
        },
    });
