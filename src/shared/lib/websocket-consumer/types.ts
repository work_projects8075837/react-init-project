export interface Message<T, D> {
    type: T;
    data: D;
}

export type WebsocketHandler<D> = (data: D) => void;
