import { BASE_DOMAIN } from '@/shared/config/constants';
import { cookieStorage, isDevDomain } from '..';
import { CredentialsStorage } from '../api-manager/types';
import { Message, WebsocketHandler } from './types';

class WebsocketConsumerBuilder<
    TCredentialsStorage extends CredentialsStorage = CredentialsStorage,
> {
    private _websocketUrl;
    constructor(
        _BASE_DOMAIN: string,
        private _credentialsStorage: TCredentialsStorage,
    ) {
        this._websocketUrl = `${
            isDevDomain(_BASE_DOMAIN) ? 'ws' : 'wss'
        }://${_BASE_DOMAIN}`;
    }

    createWebsocketConsumer = <
        T extends string,
        M extends Message<T, M['data']>,
    >({
        route = 'ws',
        handlers,
    }: {
        route?: string;
        handlers: {
            [K in T]: WebsocketHandler<Extract<M, { type: K }>['data']>;
        };
    }) => {
        let ws: WebSocket | null = null;
        const openHandler = () => {
            console.log('Websocket connection Open');
        };

        const cleanup = () => {
            console.log('Websocket connection Closed');
            if (!ws) return;
            ws.removeEventListener('open', openHandler);
            ws.removeEventListener('close', closeHandler);
            ws.removeEventListener('message', messageHandler);
            ws.close();
        };

        const closeHandler = () => {
            cleanup();
            setTimeout(createConnection, 3000);
        };

        const messageHandler = (event: MessageEvent<string>) => {
            try {
                const message: M = JSON.parse(
                    event.data
                        .replaceAll("'", '"')
                        .replaceAll('True', 'true')
                        .replaceAll('False', 'false')
                        .replaceAll('None', 'null'),
                );
                console.log(message);

                const type = message.type;

                if (type) {
                    for (const key in handlers) {
                        if (key === message.type) {
                            handlers[key](message.data);
                        }
                    }
                }
            } catch (e) {
                console.log(e);
                console.log(event.data);
            }
        };

        const createConnection = () => {
            if (ws) cleanup();

            ws = new WebSocket(
                `${
                    this._websocketUrl
                }/${route}?token=${this._credentialsStorage.getItem('access')}`,
            );

            ws.addEventListener('open', openHandler);
            ws.addEventListener('message', messageHandler);
            ws.addEventListener('close', closeHandler);
        };

        return {
            ws,
            openHandler,
            close,
            cleanup,
            createConnection,
            messageHandler,
        };
    };
}

export const websocketConsumerBuilder = new WebsocketConsumerBuilder(
    BASE_DOMAIN,
    cookieStorage,
);
