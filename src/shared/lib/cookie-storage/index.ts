export { CookiesStorage, cookieStorage } from './cookie-storage';
export type { IAppCookies, TCookiesStorage } from './cookie-storage';
