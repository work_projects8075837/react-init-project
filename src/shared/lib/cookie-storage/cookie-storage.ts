import { Cookies, ICookieData } from './types';

export class CookiesStorage<TCookieData extends ICookieData> {
    getCookies() {
        const cookies: Cookies = document.cookie
            .split('; ')
            .map((cookString) => cookString.split('='));
        let data = {} as TCookieData;

        cookies.forEach((cookie) => {
            data = { ...data, ...{ [cookie[0]]: cookie[1] } };
        });

        return data;
    }

    getItem(cookieName: string) {
        const data = this.getCookies();
        return data[cookieName];
    }

    setItem(cookieName: string, cookieValue: string) {
        const DateNow = new Date();

        const Day = DateNow.getDate();
        const Month = DateNow.getMonth();
        const Year = DateNow.getFullYear();

        const expires: string = new Date(Year, Month, Day + 10).toUTCString();

        document.cookie = `${cookieName as string}=${cookieValue}; domain=${
            window.location.hostname.split(':')[0]
        }; expires=${expires}; path=/;`;
    }

    removeItem(cookieName: string) {
        const expires: string = new Date().toUTCString();

        document.cookie = `${cookieName}=${null}; domain=${
            window.location.hostname.split(':')[0]
        }; expires=${expires}; path=/;`;
    }

    removeAllItems() {
        const cookieNames = Object.keys(this.getCookies());

        cookieNames.forEach((cookie) => this.removeItem(cookie));
    }
}

export interface IAppCookies {
    refresh: string;
    access: string;
}

export type TCookiesStorage = CookiesStorage<Partial<IAppCookies>>;

export const cookieStorage = new CookiesStorage<Partial<IAppCookies>>();
