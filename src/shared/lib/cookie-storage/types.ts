export interface ICookieData {
    [key: string]: string | undefined;
}

export type CookiePair = string[];
export type Cookies = CookiePair[];
