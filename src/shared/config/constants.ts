export const BASE_DOMAIN = 'v2.dedibyte.ru';

export const GET_AUTHORIZATION_KEY = 'GET_AUTHORIZATION_KEY';

export const ALL_ROLES = 'ALL_ROLES';
export const ADMIN_ROLE = 'Admin';
export const USER_ROLE = 'User';
