export interface SignInDto {
    email: string;
    password: string;
}

export interface SignInData {
    access: string;
    refresh: string;
}

export interface SignUpDto {
    email: string;
    password: string;
    phone: string;
    name: string;
}
