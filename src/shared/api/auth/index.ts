import { apiManager } from '@/shared/lib';
import { User } from '@/shared/model';
import { SignInData, SignInDto, SignUpDto } from './types';

export class AuthApi<TUser, TUpdateUserDto = Partial<TUser>> {
    async signIn(data: SignInDto) {
        return await apiManager.post<SignInData>('/auth/login', data);
    }

    async signUp(data: SignUpDto) {
        return await apiManager.post('/auth/signup/', data);
    }

    async getUser() {
        return await apiManager.get<TUser>('/auth/me/', {
            isAuth: true,
        });
    }

    async updateUser(data: TUpdateUserDto) {
        return await apiManager.patch<TUser>('/auth/me/', data, {
            isAuth: true,
        });
    }
}

export const authApi = new AuthApi<User>();
