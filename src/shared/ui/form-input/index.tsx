import { StyleInput } from '@/shared/style';
import classNames from 'classnames';
import React from 'react';
import { RegisterOptions, useFormContext } from 'react-hook-form';

interface FormInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
    name: string;
    options?: RegisterOptions;
    errorClass?: string;
    wrapperClass?: string;
}

const FormInputFc: React.FC<FormInputProps> = ({
    name,
    options,
    errorClass = StyleInput.errorInput,
    wrapperClass,
    ...inputProps
}) => {
    const { formState, getFieldState, register } = useFormContext();
    const { error, isTouched } = getFieldState(name, formState);

    const isError = !!error && isTouched;

    return (
        <>
            <div className={classNames(wrapperClass)}>
                <input
                    type='text'
                    {...{
                        ...inputProps,
                        name,
                        className:
                            `${StyleInput.input} ${
                                inputProps.className || ''
                            }` + (isError ? ` ${errorClass}` : ''),
                    }}
                    {...register(name, {
                        required: 'Это обязательное поле',
                        ...options,
                    })}
                />
                {isError && (
                    <span className={StyleInput.errorMessage}>
                        {error.message}
                    </span>
                )}
            </div>
        </>
    );
};

export const FormInput = React.memo(FormInputFc);
