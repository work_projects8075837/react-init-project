import { FormHTMLAttributes } from 'react';
import { FieldValues, FormProviderProps } from 'react-hook-form';

interface BaseFormProps<
    TFieldValues extends FieldValues,
    TContext,
    TTransformedValues extends FieldValues,
> extends FormProviderProps<TFieldValues, TContext, TTransformedValues> {
    outerFormChildren?: React.ReactNode;
    htmlFormProps?: FormHTMLAttributes<HTMLFormElement>;
    submit: SubmitContextData;
}

export type BaseFormComponent = <
    TFieldValues extends FieldValues,
    TContext,
    TTransformedValues extends FieldValues,
>(
    props: BaseFormProps<TFieldValues, TContext, TTransformedValues>,
) => React.ReactNode;

export type SubmitContextData = {
    isDisabled?: boolean;
    onSubmit?: () => Promise<void>;
};
