import { useContext } from 'react';
import { FieldValues, UseFormProps, useForm } from 'react-hook-form';
import { SubmitContext } from '../ui/submit-provider';

export const useBaseForm = <FieldsType extends FieldValues>(
    useFormProps?: UseFormProps<FieldsType>,
) => {
    const form = useForm<FieldsType>({ ...useFormProps, mode: 'onChange' });

    return { form };
};

export const useFormSubmit = () => {
    return useContext(SubmitContext);
};
