import { FormProvider } from 'react-hook-form';
import { BaseFormComponent } from '../types';
import { SubmitProvider } from './submit-provider';

export const BaseForm: BaseFormComponent = ({
    children,
    outerFormChildren,
    htmlFormProps,
    submit,
    ...formProps
}) => {
    return (
        <>
            <FormProvider {...formProps}>
                <form
                    {...htmlFormProps}
                    onSubmit={(event) => {
                        event.preventDefault();
                        if (htmlFormProps?.onSubmit) {
                            htmlFormProps.onSubmit(event);
                        }
                    }}
                >
                    <SubmitProvider {...submit}>{children}</SubmitProvider>
                </form>
                {outerFormChildren}
            </FormProvider>
        </>
    );
};
