import React, { createContext } from 'react';
import { SubmitContextData } from '../types';

export const SubmitContext = createContext<SubmitContextData>({});

interface SubmitProviderProps extends SubmitContextData {
    children: React.ReactNode;
}

export const SubmitProvider: React.FC<SubmitProviderProps> = ({
    children,
    isDisabled,
    onSubmit,
}) => {
    return (
        <>
            <SubmitContext.Provider value={{ isDisabled, onSubmit }}>
                {children}
            </SubmitContext.Provider>
        </>
    );
};
