import { wsEventsConsumer } from '@/shared/lib';
import { useAuthorizationQuery } from '@/shared/model';
import React, { useEffect } from 'react';

interface WebsocketProviderProps {
    children: React.ReactNode;
}

export const WebsocketProvider: React.FC<WebsocketProviderProps> = ({
    children,
}) => {
    const { data } = useAuthorizationQuery();

    useEffect(() => {
        if (data?.userId) {
            wsEventsConsumer.createConnection();
        }
        return () => wsEventsConsumer.cleanup();
    }, [!!data]);
    return <>{children}</>;
};
