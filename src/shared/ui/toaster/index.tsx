import React from 'react';
import { Toaster } from 'react-hot-toast';
import style from './style.module.scss';

const ToasterWithOptionsFc: React.FC = () => {
    return (
        <>
            <Toaster toastOptions={{ className: style.toast }} />
        </>
    );
};

export const ToasterWithOptions = React.memo(ToasterWithOptionsFc);
