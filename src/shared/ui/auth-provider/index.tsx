import { useAuthorizationQuery } from '@/shared/model';
import React from 'react';
import { WebsocketProvider } from '../websocket-provider';

interface AuthProviderProps {
    children: React.ReactNode;
    allowedRoles?: string[];
}

export const AuthProvider: React.FC<AuthProviderProps> = ({
    children,
    allowedRoles,
}) => {
    const { isLoading } = useAuthorizationQuery(allowedRoles);
    return (
        <>
            {!isLoading ? (
                <WebsocketProvider>{children}</WebsocketProvider>
            ) : (
                <span>Загрузка...</span>
            )}
        </>
    );
};
