export interface PaginatedData<T> {
    next: string | null;
    prev: string | null;
    pages: number;
    data: T;
}
