import { GET_AUTHORIZATION_KEY } from '@/shared/config/constants';
import { queryClient } from '@/shared/lib';
import { AuthorizationHookData } from './types';

export const getUserId = () => {
    return queryClient.getQueryData<AuthorizationHookData>([
        GET_AUTHORIZATION_KEY,
    ])?.userId;
};
