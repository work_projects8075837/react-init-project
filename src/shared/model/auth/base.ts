import { ALL_ROLES, GET_AUTHORIZATION_KEY } from '@/shared/config/constants';
import { useQuery } from '@tanstack/react-query';
import toast from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import { ApiManager, parseJwt } from '../../lib';

export const createAuthorizationQueryHook =
    (apiManager: ApiManager) =>
    (allowedRoles = [ALL_ROLES]) => {
        const navigate = useNavigate();
        const credentialsStorage = apiManager.getCredentialsStorage();
        const query = useQuery({
            queryKey: [GET_AUTHORIZATION_KEY],
            queryFn: async () => {
                let access: string | undefined;
                if (credentialsStorage.getItem('refresh')) {
                    access = await apiManager.authorize().catch((error) => {
                        navigate('/login/');
                        throw error;
                    });
                    credentialsStorage.setItem('access', access);
                } else {
                    access = credentialsStorage.getItem('access');
                    if (!access) {
                        navigate('/login/');
                        throw new Error();
                    }
                }

                const { sub, roles } = parseJwt(access);

                if (
                    roles &&
                    !allowedRoles.includes(ALL_ROLES) &&
                    !roles.some((role) => allowedRoles.includes(role))
                ) {
                    toast.error('Нет права на доступ');
                    navigate('/');
                }

                return { userId: sub, roles };
            },
            refetchIntervalInBackground: false,
            refetchOnWindowFocus: false,
            refetchOnMount: false,
            staleTime: Infinity,
        });

        const checkRoles = (checkRoles: string[]) => {
            return query.data?.roles || !checkRoles.includes(ALL_ROLES)
                ? query.data?.roles?.some((role) => checkRoles.includes(role))
                : true;
        };

        return { ...query, checkRoles };
    };
