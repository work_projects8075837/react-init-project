import { apiManager } from '@/shared/lib';
import { createAuthorizationQueryHook } from './base';

export const useAuthorizationQuery = createAuthorizationQueryHook(apiManager);
