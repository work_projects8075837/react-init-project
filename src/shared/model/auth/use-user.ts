import { authApi } from '@/shared/api';
import { ServiceHookOptions, useAuthQuery } from '@/shared/lib';

export const GET_USER_KEY = 'GET_USER_KEY';

export const useUser = (options: ServiceHookOptions) => {
    return useAuthQuery({
        queryFn: async () => {
            return await authApi.getUser().then((res) => res.data);
        },
        mainKey: GET_USER_KEY,
        ...options,
    });
};
